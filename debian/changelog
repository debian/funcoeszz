funcoeszz (21.1-1) unstable; urgency=medium

  * New upstream version 21.1
  * Bumped DH level to 13.
  * Bumped Standards-Version to 4.5.1.
  * debian/control:
      - Added 'bc' into Depends.
      - Fixed a Typo in the long description.
  * debian/copyright: updated license text and copyright years.
  * debian/rules: updated to install the complete version.
  * debian/tests/control: updated to improve CI tests.
  * debian/upstream/metadata: Full updated.

 -- Giovani Augusto Ferreira <giovani@debian.org>  Sat, 30 Jan 2021 14:28:09 -0300

funcoeszz (18.3-1) unstable; urgency=medium

  * Salvage package. New Maintainer. (Closes: #947315)
  * New upstream version 18.3 (Closes: #940829)
  * Using new DH level format. Consequently:
      - debian/compat: removed.
      - debian/control: Changed from 'debhelper' to 'debhelper-compat' in
        Build-Depends field and bumped level to 12.
  * debian/control:
      - Added VCS fields to use Salsa. (Closes: #912432)
      - Bumped Standards-Version to 4.4.1.
      - Improved the long description. (Closes: #940830)
      - Moved the Homepage field to Source section.
      - Set "Rules-Requires-Root: binary-targets".
      - Set "Multi-Arch: foreign" for binary package.
  * debian/copyright:
      - Using 1.0 format.
      - Full updated.
  * debian/funcoeszz.manpages: now unnecessary to install manpage, removed.
  * debian/gbp.conf: unnecessary on new vcs, removed
  * debian/lintian-overrides:
      - Created to override lintian errors in the upstream code.
  * debian/patches/fixmanpageheader.patch:
      - Renamed to 100_fixmanpageheader.patch.
      - Updated to new manpage.
  * debian/rules:
      - Added override to install correctly new manpage. (Closes: #647348)
  * debian/salsa-ci.yml: added to provide CI tests for Salsa.
  * debian/tests/control: created to provide trivial tests.
  * debian/upstream/metadata: created.
  * debian/watch: created.
  * wrap-and-sort -sa

 -- Giovani Augusto Ferreira <giovani@debian.org>  Sun, 12 Jan 2020 16:45:49 -0300

funcoeszz (15.5-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * debian/control: changed from lynx-cur to lynx in Depends field.
    (Closes: #864432)

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Tue, 08 Jan 2019 16:31:13 -0200

funcoeszz (15.5-1) unstable; urgency=medium

  [ Eder Diego de Lima Marques ]
  * [5752d12] Removing the unnecessary dependency of bash
    (essential package)
  * [012789c] Fixing header manpages
  * [7fa375e] add src fmt
  * [93aea09] Removing the .PU line
  * [01df5d5] Adding ${misc:Depends} dependency
  * Adding the README.debian file (Closes: #499011)
    Thanks to ASD Consultoria and Beraldo Leal
  * Changing dependency from the transitional package lynx to lynx-cur
    Thanks to Rafał Członka (Closes: #506466)
  * Adding bash as a dependency.
    Thanks to Ritesh Raj Sarraf
  * Adjusting the package description
  * New upstream version (15.5) (Closes: #683546)
  * Updated the manpage with the upstream version
  * Replacing the old rules files with the new format
  * Adding this file to handle the manpage in the debian way,
    without needing override dh_installman anymore
  * The manpage is now being provided by the mainstream.
  * Change the maintainer name and the debhelper version

  [ Ritesh Raj Sarraf ]
  * [ce85785] Add gbp.conf and not use pristine-tar

 -- Eder Diego de Lima Marques <eder@edermarques.net>  Fri, 21 Aug 2015 19:58:07 +0200

funcoeszz (8.3-2) unstable; urgency=low

  *  debian/control:
      - Bump Standards-Version to 3.8.0 (without modifications).
  * debian/funcoeszz.1:
     - translated to english (Closes: #486326)
     - removed reference to /usr/share/doc/funcoeszz/README.Debian

 -- Eder L. Marques <eder@edermarques.net>  Wed, 06 Aug 2008 09:41:59 -0300

funcoeszz (8.3-1) unstable; urgency=low

  * Initial release (Closes: #482096)

 -- Eder L. Marques <eder@edermarques.net>  Wed, 14 May 2008 17:47:58 -0300
